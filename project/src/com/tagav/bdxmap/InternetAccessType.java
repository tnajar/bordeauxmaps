package com.tagav.bdxmap;

public enum InternetAccessType {
	WIFI_ONLY,
	PHYSICAL_ONLY,
	WIFI_AND_PHYSICAL
}
