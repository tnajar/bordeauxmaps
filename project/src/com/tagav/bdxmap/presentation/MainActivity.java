package com.tagav.bdxmap.presentation;

import java.util.ArrayList;
import java.util.Set;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.ActivityInfo;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Build;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.tagav.bdxmap.R;
import com.tagav.bdxmap.business.InternetAccessPointsBusiness;
import com.tagav.bdxmap.business.PlaygroundsBusiness;
import com.tagav.bdxmap.business.PublicParkingLotsBusiness;
import com.tagav.bdxmap.business.PublicToiletsBusiness;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public class MainActivity extends Activity {
	private static final String DIALOG_TITLE = "Ajout d'un emplacement personnalisé";
	private static final String MESSAGE_NO_CONNECTION = "Vérifier votre connexion et réessayez";
	private static final String MESSAGE_SERVER_ERROR = "Un incident serveur est survenu, l'application a été fermée";

	private static final double INITIAL_LATITUDE = 44.833;
	private static final double INITIAL_LONGITUDE = -0.567;
	private static final float INITIAL_ZOOM = 13;
	private static final String MY_PREFERENCES = "BdxPref";
	private static final String DISPLAYED_MARKERS = "displayedMarkers";
	private static final String CUSTOM_MARKERS = "customMarkers";
	private GoogleMap mMap;
	private static final int NB_MARKERS = 4;
	private static final int PLAYGROUNDS_MARKER = 0;
	private static final int PARKING_MARKER = 1;
	private static final int TOILETS_MARKER = 2;
	private static final int INTERNET_MARKER = 3;

	private EditText titleText;
	private EditText descriptionText;
	private Dialog newMarkerdialog;
	private CustomMarkersHandler customMarkers;
	private ArrayList<MarkersHandler> markerHandlers;
	private ArrayList<ToggleButton> buttons;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		boolean isPortraitMode = getResources().getBoolean(
				R.bool.portrait_orientation);
		if (isPortraitMode) {
			this.setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
		} else {
			setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
		}
		buttons = initializeButtons();
		if (isNetworkAvailable()) {
			setUpMap();
			try {
				markerHandlers = initializeMarkerHolders(mMap);
			} catch (DAOException e) {
				showToastErrorAndExit(MESSAGE_SERVER_ERROR);
			}
			restorePreferences(mMap);
		} else {
			showToastErrorAndExit(MESSAGE_NO_CONNECTION);
		}
	}

	private boolean isNetworkAvailable() {
		ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
		NetworkInfo activeNetworkInfo = connectivityManager
				.getActiveNetworkInfo();
		return activeNetworkInfo != null;
	}

	private void showToastErrorAndExit(String message) {
		Toast toast = Toast.makeText(getApplicationContext(), message,
				Toast.LENGTH_LONG);
		toast.show();
		finish();
	}

	@Override
	protected void onStop() {
		super.onStop();
		savePreferences();
	}

	private void savePreferences() {
		if (markerHandlers == null) {
			return;
		}
		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = settings.edit();
		StringBuilder sb = new StringBuilder();
		for (int i = 0; i < NB_MARKERS; i++) {
			sb.append(markerHandlers.get(i).getStatus() ? 1 : 0);
		}
		editor.putString(DISPLAYED_MARKERS, sb.toString());
		Set<String> markers = customMarkers.getMarkersAsString();
		editor.putStringSet(CUSTOM_MARKERS, markers);
		editor.commit();
	}

	private void restorePreferences(GoogleMap map) {
		if (markerHandlers == null) {
			return;
		}
		SharedPreferences settings = getSharedPreferences(MY_PREFERENCES,
				Context.MODE_PRIVATE);
		String defaultSettings = new String(new char[NB_MARKERS]).replace('\0',
				'0');
		String markersStatus = settings.getString(DISPLAYED_MARKERS,
				defaultSettings);
		for (int i = 0; i < NB_MARKERS; i++) {
			boolean status = (markersStatus.toCharArray()[i] == '0') ? false
					: true;
			markerHandlers.get(i).changeStatus(status);
			buttons.get(i).setChecked(status);
		}
		Set<String> markers = settings.getStringSet(CUSTOM_MARKERS, null);
		customMarkers = new CustomMarkersHandler(map, markers);
	}

	private ArrayList<MarkersHandler> initializeMarkerHolders(GoogleMap map)
			throws DAOException {
		ArrayList<MarkersHandler> result = new ArrayList<MarkersHandler>();
		result.add(new MarkersHandler(map, new PlaygroundsBusiness()
				.getAllElements(), MarkersHandler.PLAYGROUND_COLOR));
		result.add(new MarkersHandler(map, new PublicParkingLotsBusiness()
				.getAllElements(), MarkersHandler.PARKING_COLOR));
		result.add(new MarkersHandler(map, new PublicToiletsBusiness()
				.getAllElements(), MarkersHandler.TOILET_COLOR));
		result.add(new MarkersHandler(map, new InternetAccessPointsBusiness()
				.getAllElements(), MarkersHandler.INTERNET_COLOR));
		return result;
	}

	private void setUpMap() {
		if (mMap == null) {
			mMap = ((MapFragment) getFragmentManager().findFragmentById(
					R.id.map)).getMap();
			if (mMap != null) {
				mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(
						INITIAL_LATITUDE, INITIAL_LONGITUDE), INITIAL_ZOOM));
				mMap.setMyLocationEnabled(true);
				mMap.getUiSettings().setTiltGesturesEnabled(false);
				mMap.getUiSettings().setZoomControlsEnabled(false);
				mMap.setOnMapLongClickListener(new GoogleMap.OnMapLongClickListener() {

					@Override
					public void onMapLongClick(LatLng position) {
						showPopUpNewMarker(position);
					}
				});
			}
		}
	}

	private void showPopUpNewMarker(final LatLng position) {
		Button button = initializeCustomMarkerDialog();
		newMarkerdialog.show();
		button.setOnClickListener(new View.OnClickListener() {

			@Override
			public void onClick(View v) {
				String title = titleText.getText().toString();
				String desc = descriptionText.getText().toString();
				customMarkers.addMarker(position, title, desc);
				newMarkerdialog.dismiss();
			}
		});
	}

	private Button initializeCustomMarkerDialog() {
		newMarkerdialog = new Dialog(this);
		newMarkerdialog.setContentView(R.layout.form_new_marker);
		newMarkerdialog.setCancelable(true);
		newMarkerdialog.setTitle(DIALOG_TITLE);

		Button button = (Button) newMarkerdialog
				.findViewById(R.id.form_b_button);
		titleText = (EditText) newMarkerdialog.findViewById(R.id.form_et_title);
		descriptionText = (EditText) newMarkerdialog
				.findViewById(R.id.form_et_desc);
		InputFilter filter = new InputFilter() {
			@TargetApi(Build.VERSION_CODES.KITKAT)
			@Override
			public CharSequence filter(CharSequence source, int start, int end,
					Spanned dest, int dstart, int dend) {
				for (int i = start; i < end; i++) {
					if (Character.compare(source.charAt(i),
							CustomMarkersHandler.SPLITTER) == 0) {
						return "";
					}
				}
				return null;
			}
		};
		descriptionText.setFilters(new InputFilter[] { filter });
		titleText.setFilters(new InputFilter[] { filter });
		return button;
	}

	private void onClickMarkerHandler(int position) {
		MarkersHandler mHandler = markerHandlers.get(position);
		if (mHandler.getStatus()) {
			mHandler.changeStatus(false);
		} else {
			mHandler.changeStatus(true);
		}
	}

	public void onClickInternet(View v) {
		onClickMarkerHandler(INTERNET_MARKER);
	}

	public void onClickPlaygrounds(View v) {
		onClickMarkerHandler(PLAYGROUNDS_MARKER);
	}

	public void onClickToilets(View v) {
		onClickMarkerHandler(TOILETS_MARKER);
	}

	public void onClickParking(View v) {
		onClickMarkerHandler(PARKING_MARKER);
	}

	public void onClickPlus(View v) {
		showPopUpNewMarker(mMap.getCameraPosition().target);
	}

	private ArrayList<ToggleButton> initializeButtons() {
		ArrayList<ToggleButton> results = new ArrayList<ToggleButton>();
		results.add((ToggleButton) findViewById(R.id.main_activity_button_playgrounds));
		results.add((ToggleButton) findViewById(R.id.main_activity_button_parkinglots));
		results.add((ToggleButton) findViewById(R.id.main_activity_button_toilets));
		results.add((ToggleButton) findViewById(R.id.main_activity_button_internet));
		return results;
	}
}
