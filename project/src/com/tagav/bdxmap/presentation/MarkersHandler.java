package com.tagav.bdxmap.presentation;

import java.util.ArrayList;
import java.util.List;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.tagav.bdxmap.business.element.CommonMapElement;

public class MarkersHandler {
	protected GoogleMap mMap = null;
	protected boolean buttonIsOn;
	protected List<Marker> markersList = null;
	protected float color;
	public static final float PLAYGROUND_COLOR = 120.0f;
	public static final float INTERNET_COLOR = 26.0f;
	public static final float TOILET_COLOR = 168.0f;
	public static final float PARKING_COLOR = 216.0f;	

	protected MarkersHandler(GoogleMap map, float color) {
		this.mMap = map;
		this.color = color;
		this.markersList = new ArrayList<Marker>();
		this.buttonIsOn = true;
	}

	public MarkersHandler(GoogleMap map, List<CommonMapElement> l, float color) {
		this.mMap = map;
		this.color = color;
		this.markersList = loadMarkers(l);
	}

	public void removeMarkers() {
		for (Marker marker : markersList) {
			marker.remove();
		}
		markersList = new ArrayList<Marker>();
	}

	public void hideMarkers() {
		for (Marker marker : markersList) {
			marker.setVisible(false);
		}
	}

	public void showMarkers() {
		for (Marker marker : markersList) {
			marker.setVisible(true);
		}
	}

	private List<Marker> loadMarkers(List<CommonMapElement> list) {
		List<Marker> markers = new ArrayList<Marker>();
		for (CommonMapElement elt : list) {
			LatLng position = new LatLng(elt.getLattitude(), elt.getLongitude());
			Marker marker = mMap.addMarker(new MarkerOptions().title(elt.getTitle())
					.position(position)
					.snippet(elt.getDescription())
					.visible(false)
					.icon(BitmapDescriptorFactory.defaultMarker(color)));
			markers.add(marker);
		}
		return markers;
	}

	public void changeStatus(boolean s) {
		buttonIsOn = s;
		if (buttonIsOn) {
			showMarkers();
		} else {
			hideMarkers();
		}
		
	}
	
	public boolean getStatus() {
		return buttonIsOn;
	}
}
