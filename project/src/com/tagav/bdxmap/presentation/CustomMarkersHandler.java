package com.tagav.bdxmap.presentation;

import java.util.HashSet;
import java.util.Set;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

public class CustomMarkersHandler extends MarkersHandler {
	public static final char SPLITTER = '#';
	public CustomMarkersHandler(GoogleMap map, Set<String> elements) {
		super(map, 48.0f);
		if (elements != null) {
			for (String s : elements) {
				addFromString(s);
			}
		}
	}

	public void addMarker(LatLng position, String title, String description) {
		Marker marker = mMap.addMarker(new MarkerOptions().title(title)
				.position(position)
				.snippet(description)
				.draggable(true)
				.icon(BitmapDescriptorFactory.defaultMarker(color)));
		markersList.add(marker);
	}

	public Set<String> getMarkersAsString() {
		Set<String> result = new HashSet<String>();
		for (Marker marker : markersList) {
			LatLng position = marker.getPosition();
			String splitter = String.valueOf(SPLITTER);
			StringBuilder sb = new StringBuilder();
			sb.append(position.latitude + splitter);
			sb.append(position.longitude + splitter);
			sb.append(marker.getTitle() + splitter);
			sb.append(marker.getSnippet());
			result.add(sb.toString());
		}
		return result;
	}

	private void addFromString(String s) {
		String[] tokens = s.split(String.valueOf(SPLITTER));
		if (tokens.length != 4) {
			return;
		} else {
			LatLng position = null;
			try {
				position = new LatLng(Double.valueOf(tokens[0]), Double.valueOf(tokens[1]));
			} catch(NumberFormatException e) {
				return;
			}
			addMarker(position, tokens[2], tokens[3]);
		}
	}

}
