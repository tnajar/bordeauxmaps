package com.tagav.bdxmap;

public enum ParkingType {
	FREE_INDOOR,
	FREE_OUTDOOR,
	PAYING_INDOOR,
	PAYING_OUTDOOR,
}
