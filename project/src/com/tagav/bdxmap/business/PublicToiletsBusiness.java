package com.tagav.bdxmap.business;

import java.util.ArrayList;

import com.tagav.bdxmap.ToiletType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.PublicToilet;
import com.tagav.bdxmap.converter.PublicToiletsConverter;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public class PublicToiletsBusiness extends CommonBusiness {

	public PublicToiletsBusiness() throws DAOException {
		PublicToiletsConverter c = new PublicToiletsConverter();
		elements = c.businessFactory();
	}

	void debugInit() {
		elements = new ArrayList<CommonMapElement>();
		elements.add(new PublicToilet("FRANTZ SCHRADER", ToiletType.URINAL,
				false, -0.607806284750287, 44.835185610133));
		elements.add(new PublicToilet("André Meunier", ToiletType.URINAL,
				false, -0.562917322479597, 44.829530512184));
		elements.add(new PublicToilet("Quai des Chartrons (Guinguette)",
				ToiletType.AUTOMATIC_TOILET, true, -0.570091226378448,
				44.8491422413001));
	}
}
