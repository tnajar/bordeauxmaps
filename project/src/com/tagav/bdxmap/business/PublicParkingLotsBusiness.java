package com.tagav.bdxmap.business;

import java.util.ArrayList;

import com.tagav.bdxmap.ParkingType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.PublicParkingLot;
import com.tagav.bdxmap.converter.PublicParkingLotsConverter;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public class PublicParkingLotsBusiness extends CommonBusiness {

	public PublicParkingLotsBusiness() throws DAOException {
		PublicParkingLotsConverter c = new PublicParkingLotsConverter();
		elements = c.businessFactory();
	}

	void debugInit() {
		elements = new ArrayList<CommonMapElement>();
		elements.add(new PublicParkingLot("masson", 27,
				ParkingType.FREE_OUTDOOR, -0.614855852446046, 44.8547709781248));
		elements.add(new PublicParkingLot("Alsace-Lorraine", 27,
				ParkingType.PAYING_OUTDOOR, -0.568466979836578, 44.837661119745));
		elements.add(new PublicParkingLot("barreyre", 30,
				ParkingType.FREE_OUTDOOR, -0.568537395987252, 44.8552461227317));
	}
}
