package com.tagav.bdxmap.business.element;

import com.tagav.bdxmap.InternetAccessType;

public class InternetAccessPoint extends CommonMapElement {
	private static final String SNIPPET_FREE = "Gratuit";
	private static final String SNIPPET_PAID = "Payant";
	private static final String SNIPPET_PHYSICAL_ONLY = "Postes";
	private static final String SNIPPET_WIFI_ONLY = "Accès wifi";
	private static final String SNIPPET_BOTH = "Accès sans-fil et postes";
	private String name;
	private boolean free;
	private InternetAccessType type;

	public InternetAccessPoint(String name, boolean free,
			InternetAccessType type, double lat, double lng) {
		super(lat, lng);
		this.name = name;
		this.free = free;
		this.type = type;
	}

	@Override
	public String getTitle() {
		return name;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder();
		if (free) {
			sb.append(SNIPPET_FREE + SNIPPET_NEW_LINE);
		} else {
			sb.append(SNIPPET_PAID + SNIPPET_NEW_LINE);
		}
		if (type == null) {
			return sb.toString();
		}
		switch (type) {
		case WIFI_ONLY:
			sb.append(SNIPPET_WIFI_ONLY);
			break;
		case PHYSICAL_ONLY:
			sb.append(SNIPPET_PHYSICAL_ONLY);
			break;
		case WIFI_AND_PHYSICAL:
			sb.append(SNIPPET_BOTH );
			break;
		default:
			break;
		}
		return sb.toString();
	}
}
