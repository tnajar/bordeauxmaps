package com.tagav.bdxmap.business.element;

import com.tagav.bdxmap.ParkingType;

public class PublicParkingLot extends CommonMapElement {

	private static final String SNIPPET_PAID_OUTDOOR = "payant / en extérieur";
	private static final String SNIPPET_PAID_INDOOR = "payant / en intérieur";
	private static final String SNIPPET_FREE_OUTDOOR = "gratuit / en extérieur";
	private static final String SNIPPET_FREE_INDOOR = "gratuit / en intérieur";
	private static final String SNIPPET_CAPACITY = " places";
	private String name;
	private int capacity;
	private ParkingType type;

	public PublicParkingLot(String name, int capacity, ParkingType type,
			double lat, double lng) {
		super(lat, lng);
		this.name = name;
		this.capacity = capacity;
		this.type = type;
	}

	@Override
	public String getTitle() {
		return name;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder("");
		if (capacity != 0) {
			sb.append(capacity + SNIPPET_CAPACITY + SNIPPET_NEW_LINE);
		}
		if (type != null) {
			sb.append(typeToString(type));
		}
		return sb.toString();
	}

	private String typeToString(ParkingType type) {
		if (type == null) {
			return "";
		}
		switch (type) {
		case FREE_INDOOR:
			return SNIPPET_FREE_INDOOR;
		case FREE_OUTDOOR:
			return SNIPPET_FREE_OUTDOOR;
		case PAYING_INDOOR:
			return SNIPPET_PAID_INDOOR;
		case PAYING_OUTDOOR:
			return SNIPPET_PAID_OUTDOOR;
		default:
			return "";
		}
	}
}
