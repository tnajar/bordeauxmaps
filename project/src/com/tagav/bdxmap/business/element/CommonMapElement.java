package com.tagav.bdxmap.business.element;

public abstract class CommonMapElement  {
	protected double lng;
	protected double lat;
	protected static final String SNIPPET_NEW_LINE = " / ";
	protected static final String SNIPPET_HYPHEN = " - ";

	protected CommonMapElement(double lat, double lng) {
		super();
		this.lng = lng;
		this.lat = lat;
	}
	
	public double getLattitude() {
		return lng;
	}
	public double getLongitude() {
		return lat;
	}

	public abstract String getTitle();
	public abstract String getDescription();
	
	public static void firstLetterUpperCase(String input) {
		input = Character.toUpperCase(input.charAt(0)) + input.substring(1);
	}
}
