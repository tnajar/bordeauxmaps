package com.tagav.bdxmap.business.element;

import com.tagav.bdxmap.ToiletType;

public class PublicToilet extends CommonMapElement {

	private String name;
	private ToiletType type;
	private boolean disabledAccess;
	private final static String SNIPPET_DISABLED_ACCESS = "Accès handicapé";
	private static final String SNIPPET_AUTOMATIC_TOILET = "Toilettes automatiques";
	private static final String SNIPPET_URINAL = "Urinoirs";
	private static final String SNIPPET_BOTH_TYPES = "Urinoirs et sanitaires";

	public PublicToilet(String name, ToiletType type, boolean disabledAccess,
			double lat, double lng) {
		super(lat, lng);
		this.name = name;
		this.type = type;
		this.disabledAccess = disabledAccess;
	}

	@Override
	public String getTitle() {
		return name;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder();
		if (disabledAccess) {
			sb.append(SNIPPET_DISABLED_ACCESS + SNIPPET_NEW_LINE);
		}
		sb.append(typeToString(type));
		return sb.toString();
	}

	public static String typeToString(ToiletType type) {
		if (type == null) {
			return "";
		}
		switch (type) {
		case AUTOMATIC_TOILET:
			return SNIPPET_AUTOMATIC_TOILET;
		case URINAL:
			return SNIPPET_URINAL;
		default:
			return SNIPPET_BOTH_TYPES;
		}
	}

}
