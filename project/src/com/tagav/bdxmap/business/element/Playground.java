package com.tagav.bdxmap.business.element;


public class Playground extends CommonMapElement {
	private static final String SNIPPET_GAMES = " jeux";
	private static final String SNIPPET_AGE = " ans";
	private String name;
	private int min_age;
	private int max_age;
	private int number_games;

	public Playground(String name, int min_age, int max_age, int number_games, double lat, double lng) {
		super(lat, lng);
		this.name = name;
		this.min_age = min_age;
		this.max_age = max_age;
		this.number_games = number_games;
	}

	@Override
	public String getTitle() {
		return name;
	}

	@Override
	public String getDescription() {
		StringBuilder sb = new StringBuilder("");
		if (number_games != 0) {
			sb.append(number_games + SNIPPET_GAMES + SNIPPET_NEW_LINE);
		}
		if (min_age > 0 && max_age > 0) {
			sb.append(min_age + SNIPPET_HYPHEN + max_age + SNIPPET_AGE);
		}
		return sb.toString();
	}
}
