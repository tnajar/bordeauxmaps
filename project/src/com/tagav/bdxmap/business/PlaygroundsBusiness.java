package com.tagav.bdxmap.business;

import java.util.ArrayList;

import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.Playground;
import com.tagav.bdxmap.converter.PlaygroundsConverter;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public class PlaygroundsBusiness extends CommonBusiness {

	public PlaygroundsBusiness() throws DAOException {
		super();
		PlaygroundsConverter c = new PlaygroundsConverter();
		elements = c.businessFactory();
	}

	@Override
	void debugInit() {
		elements = new ArrayList<CommonMapElement>();
		elements.add(new Playground("Rue du Petit Cardinal", 1, 8, 5,
				-0.545712776468848, 44.8463826366364));
		elements.add(new Playground("Place Pierre Jacques Dormoy", 1, 12, 3,
				-0.564434590185037, 44.8725408904526));
		elements.add(new Playground("Parc de la Cité des Aubiers_Bâtiment", 2,
				6, 6, -0.57102427552912, 44.8725408904526));
		elements.add(new Playground("Parc Bordelais_Déversoir", 0, 0, 0,
				-0.601930413206061, 44.852085248005));
		elements.add(new Playground("Jardin Public_Ile aux enfants", 1, 8, 10,
				-0.577146615811943, 44.8490898062911));
		elements.add(new Playground("Square Jean Bureau", 2, 12, 5,
				-0.569550040671983, 44.8366080098571));
		elements.add(new Playground("Place des Martyrs de la Résistance", 2, 8,
				5, -0.585130728407167, 44.8429422000554));
		elements.add(new Playground("Parc de la Cité des Aubiers_Pelouse", 6,
				12, 1, -0.572453534243851, 44.8719188328467));
		elements.add(new Playground("Square Liotard", 3, 11, 5,
				-0.566047870541021, 44.8127563570283));
		elements.add(new Playground("Parc de la Cité du Grand Parc_Grands", 4,
				8, 1, -0.583399365585414, 44.8601359558296));
	}

}
