package com.tagav.bdxmap.business;

import java.util.ArrayList;

import com.tagav.bdxmap.InternetAccessType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.InternetAccessPoint;
import com.tagav.bdxmap.converter.InternetAccessPointsConverter;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public class InternetAccessPointsBusiness extends CommonBusiness {

	public InternetAccessPointsBusiness() throws DAOException {
		super();
		InternetAccessPointsConverter c = new InternetAccessPointsConverter();
		elements = c.businessFactory();
	}

	void debugInit() {
		elements = new ArrayList<CommonMapElement>();
		elements.add(new InternetAccessPoint("Bibliothèque Aubiers Est", true,
				InternetAccessType.WIFI_ONLY, -0.57285579557617,
				44.8737831813051));
		elements.add(new InternetAccessPoint("Club Senior Lumineuse", false,
				InternetAccessType.PHYSICAL_ONLY, -0.57285579557617,
				44.8737831813051));
	}
}
