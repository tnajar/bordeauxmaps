package com.tagav.bdxmap.business;

import java.util.List;

import com.tagav.bdxmap.business.element.CommonMapElement;

public abstract class CommonBusiness {

	protected List<CommonMapElement> elements;

	abstract void debugInit();

	public List<CommonMapElement> getAllElements() {
		return elements;
	}
}
