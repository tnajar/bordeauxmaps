package com.tagav.bdxmap;

public enum ToiletType {
	URINAL,
	AUTOMATIC_TOILET,
	URINAL_AND_TOILET
}
