package com.tagav.bdxmap.dao.internet.dao;

import org.json.JSONObject;

import com.tagav.bdxmap.dao.internet.dto.PlaygroundInternetDTO;

public class PlaygroundsInternetDAO extends CommonInternetDAO<PlaygroundInternetDTO>{
	public PlaygroundsInternetDAO() {
		this.urlAddress = URL_PLAYGROUNDS_JSON;
	}

	@Override
	public PlaygroundInternetDTO convert(JSONObject jsonObject) {
		PlaygroundInternetDTO dto = new PlaygroundInternetDTO();
		
		try {dto.setAge_max(jsonObject.getString(AGE_MAX));} catch (Exception ignored) {}
		try {dto.setAge_min(jsonObject.getString(AGE_MIN));} catch (Exception ignored) {}
		try {dto.setCle(jsonObject.getString(CLE));} catch (Exception ignored) {}
		try {dto.setEntityid(jsonObject.getString(ID));} catch (Exception ignored) {}
		try {dto.setGeometrie(jsonObject.getString(GEOMETRIE));} catch (Exception ignored) {}
		try {dto.setNature(jsonObject.getString(NATURE));} catch (Exception ignored) {}
		try {dto.setNom(jsonObject.getString(NOM));} catch (Exception ignored) {}
		try {dto.setNombre_jeux(jsonObject.getString(NOMBRE_JEUX));} catch (Exception ignored) {}
		try {dto.setNum_quartier(jsonObject.getString(NUM_QUARTIER));} catch (Exception ignored) {}
		try {dto.setPartitionKey(jsonObject.getString(PARTITION_KEY));} catch (Exception ignored) {}
		try {dto.setRowKey(jsonObject.getString(ROW_KEY));} catch (Exception ignored) {}
		try {dto.setTimestamp(jsonObject.getString(TIMESTAMP));} catch (Exception ignored) {}
		try {dto.setX_long(jsonObject.getString(X_LONG));} catch (Exception ignored) {}
		try {dto.setY_lat(jsonObject.getString(Y_LAT));} catch (Exception ignored) {}

		return dto;
	}

}
