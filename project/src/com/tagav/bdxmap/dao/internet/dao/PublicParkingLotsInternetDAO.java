package com.tagav.bdxmap.dao.internet.dao;

import org.json.JSONObject;

import com.tagav.bdxmap.dao.internet.dto.PublicParkingLotInternetDTO;

public class PublicParkingLotsInternetDAO extends CommonInternetDAO<PublicParkingLotInternetDTO>{
	public PublicParkingLotsInternetDAO() {
		this.urlAddress = URL_PARKINGS_JSON;
	}

	@Override
	public PublicParkingLotInternetDTO convert(JSONObject jsonObject) {
		PublicParkingLotInternetDTO dto = new PublicParkingLotInternetDTO();
		
		try {dto.setCle(jsonObject.getString(CLE));} catch (Exception ignored) {}
		try {dto.setEntityid(jsonObject.getString(ID));} catch (Exception ignored) {}
		try {dto.setDomanialite(jsonObject.getString(DOMANIALITE));} catch (Exception ignored) {}
		try {dto.setGeometrie(jsonObject.getString(GEOMETRIE));} catch (Exception ignored) {}
		try {dto.setNature(jsonObject.getString(NATURE));} catch (Exception ignored) {}
		try {dto.setNom(jsonObject.getString(NOM));} catch (Exception ignored) {}
		try {dto.setNombre_de_places(jsonObject.getString(NOMBRE_DE_PLACES));} catch (Exception ignored) {}
		try {dto.setNum_quartier(jsonObject.getString(NUM_QUARTIER));} catch (Exception ignored) {}
		try {dto.setPartitionKey(jsonObject.getString(PARTITION_KEY));} catch (Exception ignored) {}
		try {dto.setRowKey(jsonObject.getString(ROW_KEY));} catch (Exception ignored) {}
		try {dto.setTimestamp(jsonObject.getString(TIMESTAMP));} catch (Exception ignored) {}
		try {dto.setX_long(jsonObject.getString(X_LONG));} catch (Exception ignored) {}
		try {dto.setY_lat(jsonObject.getString(Y_LAT));} catch (Exception ignored) {}
		return dto;
	}

}
