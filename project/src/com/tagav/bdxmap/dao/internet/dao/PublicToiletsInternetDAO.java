package com.tagav.bdxmap.dao.internet.dao;

import org.json.JSONObject;

import com.tagav.bdxmap.dao.internet.dto.PublicToiletInternetDTO;

public class PublicToiletsInternetDAO extends CommonInternetDAO<PublicToiletInternetDTO>{
	public PublicToiletsInternetDAO() {
		this.urlAddress = URL_TOILETS_JSON;
	}

	@Override
	public PublicToiletInternetDTO convert(JSONObject jsonObject) {
		PublicToiletInternetDTO dto = new PublicToiletInternetDTO();
		
		try {dto.setAdresse(jsonObject.getString(ADRESSE));} catch (Exception ignored) {}
		try {dto.setCle(jsonObject.getString(CLE));} catch (Exception ignored) {}
		try {dto.setEntityid(jsonObject.getString(ID));} catch (Exception ignored) {}
		try {dto.setGeometrie(jsonObject.getString(GEOMETRIE));} catch (Exception ignored) {}
		try {dto.setNom(jsonObject.getString(NOM));} catch (Exception ignored) {}
		try {dto.setNum_quartier(jsonObject.getString(NUM_QUARTIER));} catch (Exception ignored) {}
		try {dto.setOption(jsonObject.getString(OPTION));} catch (Exception ignored) {}
		try {dto.setPartitionKey(jsonObject.getString(PARTITION_KEY));} catch (Exception ignored) {}
		try {dto.setQuartier(jsonObject.getString(QUARTIER));} catch (Exception ignored) {}
		try {dto.setRowKey(jsonObject.getString(ROW_KEY));} catch (Exception ignored) {}
		try {dto.setTimestamp(jsonObject.getString(TIMESTAMP));} catch (Exception ignored) {}
		try {dto.setTypologie(jsonObject.getString(TYPOLOGIE));} catch (Exception ignored) {}
		try {dto.setX_long(jsonObject.getString(X_LONG));} catch (Exception ignored) {}
		try {dto.setY_lat(jsonObject.getString(Y_LAT));} catch (Exception ignored) {}

		return dto;
	}

}
