package com.tagav.bdxmap.dao.internet.dao;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;

public abstract class CommonInternetDAO<T> {
	protected static final String URL_INTERNETS_JSON = "http://odata.bordeaux.fr/v1/databordeaux/sigaccesinternet/?format=json";
	protected static final String URL_PLAYGROUNDS_JSON = "http://odata.bordeaux.fr/v1/databordeaux/airejeux/?format=json";
	protected static final String URL_PARKINGS_JSON = "http://odata.bordeaux.fr/v1/databordeaux/sigparkpub/?format=json";
	protected static final String URL_TOILETS_JSON = "http://odata.bordeaux.fr/v1/databordeaux/sigsanitaire/?format=json";

	protected static final String X_LONG = "x_long";
	protected static final String Y_LAT = "y_lat";
	protected static final String SITUATION = "situation";
	protected static final String TYPOLOGIE = "typologie";
	protected static final String TYPE_PUBLIC = "type_public";
	protected static final String TYPE_ACCES = "type_acces";
	protected static final String TIMESTAMP = "Timestamp";
	protected static final String ROW_KEY = "RowKey";
	protected static final String PAYANT = "payant";
	protected static final String PARTITION_KEY = "PartitionKey";
	protected static final String NUM_QUARTIER = "num_quartier";
	protected static final String NOM = "nom";
	protected static final String NB_POSTES = "nb_postes";
	protected static final String GEOMETRIE = "geometrie";
	protected static final String ID = "entityid";
	protected static final String CLE = "cle";
	protected static final String NOMBRE_JEUX = "nombre_jeux";
	protected static final String NATURE = "nature";
	protected static final String AGE_MIN = "age_min";
	protected static final String AGE_MAX = "age_max";
	protected static final String NOMBRE_DE_PLACES = "nombre_de_places";
	protected static final String DOMANIALITE = "domanialite";
	protected static final String QUARTIER = "quartier";
	protected static final String OPTION = "options";
	protected static final String ADRESSE = "adresse";

	protected String urlAddress;

	public abstract T convert(JSONObject jsonObject);

	public List<T> getData() throws DAOException {
		List<T> typedDTOList = new ArrayList<T>();
		AsyncTask<String, Integer, JSONArray> jsonArrayTask = new JSONArrayTask();
		try {
			JSONArray jsonArray = jsonArrayTask.execute(urlAddress).get();
			for (int i = 0; i < jsonArray.length(); i++) {
				T typedDTO = convert(jsonArray.getJSONObject(i));
				typedDTOList.add(typedDTO);
			}
		} catch (Exception e) {
			throw new DAOException("Parsing Error", e);
		}
		return typedDTOList;
	}
}

class JSONArrayTask extends AsyncTask<String, Integer, JSONArray> {

	private static final String FIRST_KEY = "d";
	private static final String UTF_8 = "UTF-8";
	private static final String GET = "GET";
	private static final int TIMEOUT = 10000;

	@Override
	protected JSONArray doInBackground(String... arg0) {
		JSONArray jsonArray = null;
		InputStream is = null;
		try {
			is = connectTo(arg0[0]);
			jsonArray = parseResponse(is);
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (is != null) {
				closeInputStream(is);
			}
		}
		return jsonArray;
	}

	private void closeInputStream(InputStream is) {
		try {
			is.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	private InputStream connectTo(String urlToParse) throws IOException {
		InputStream is;
		URL url = new URL(urlToParse);
		HttpURLConnection connexion = (HttpURLConnection) url.openConnection();
		connexion.setReadTimeout(10000);
		connexion.setConnectTimeout(TIMEOUT);
		connexion.setRequestMethod(GET);
		connexion.setDoInput(true);
		connexion.connect();
		is = connexion.getInputStream();
		return is;
	}

	private JSONArray parseResponse(InputStream is) {
		BufferedReader streamReader;
		try {
			streamReader = new BufferedReader(new InputStreamReader(is, UTF_8));
			StringBuilder responseStrBuilder = new StringBuilder();
			String inputStr;
			while ((inputStr = streamReader.readLine()) != null)
				responseStrBuilder.append(inputStr);

			JSONObject rootObject = new JSONObject(
					responseStrBuilder.toString());
			JSONArray jsonArray = rootObject.getJSONArray(FIRST_KEY);
			return jsonArray;

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (JSONException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return null;
	}
}