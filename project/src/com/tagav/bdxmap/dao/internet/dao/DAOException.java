package com.tagav.bdxmap.dao.internet.dao;

public class DAOException extends Exception {
	static final long serialVersionUID = 1;

	public DAOException(String detailMessage, Throwable throwable) {
		super(detailMessage, throwable);
	}

	public DAOException(String detailMessage) {
		super(detailMessage);
	}

	public static long getSerialversionuid() {
		return serialVersionUID;
	}
}
