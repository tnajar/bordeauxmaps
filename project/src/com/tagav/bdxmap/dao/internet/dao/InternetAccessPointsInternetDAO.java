package com.tagav.bdxmap.dao.internet.dao;

import org.json.JSONObject;

import com.tagav.bdxmap.dao.internet.dto.InternetAccessPointInternetDTO;

public class InternetAccessPointsInternetDAO extends CommonInternetDAO<InternetAccessPointInternetDTO>{
	public InternetAccessPointsInternetDAO() {
		this.urlAddress = URL_INTERNETS_JSON;
	}

	@Override
	public InternetAccessPointInternetDTO convert(JSONObject jsonObject) {
		InternetAccessPointInternetDTO dto = new InternetAccessPointInternetDTO();

		try {dto.setCle(jsonObject.getString(CLE));} catch (Exception ignored) {}
		try {dto.setEntityid(jsonObject.getString(ID));} catch (Exception ignored) {}
		try {dto.setGeometrie(jsonObject.getString(GEOMETRIE));} catch (Exception ignored) {}
		try {dto.setNb_postes(jsonObject.getString(NB_POSTES));} catch (Exception ignored) {}
		try {dto.setNom(jsonObject.getString(NOM));} catch (Exception ignored) {}
		try {dto.setNum_quartier(jsonObject.getString(NUM_QUARTIER));} catch (Exception ignored) {}
		try {dto.setPartitionKey(jsonObject.getString(PARTITION_KEY));} catch (Exception ignored) {}
		try {dto.setPayant(jsonObject.getString(PAYANT));} catch (Exception ignored) {}
		try {dto.setRowKey(jsonObject.getString(ROW_KEY));} catch (Exception ignored) {}
		try {dto.setSituation(jsonObject.getString(SITUATION));} catch (Exception ignored) {}
		try {dto.setTimestamp(jsonObject.getString(TIMESTAMP));} catch (Exception ignored) {}
		try {dto.setType_acces(jsonObject.getString(TYPE_ACCES));} catch (Exception ignored) {}
		try {dto.setType_public(jsonObject.getString(TYPE_PUBLIC));} catch (Exception ignored) {}
		try {dto.setX_long(jsonObject.getString(X_LONG));} catch (Exception ignored) {}
		try {dto.setY_lat(jsonObject.getString(Y_LAT));} catch (Exception ignored) {}

		return dto;
	}

}
