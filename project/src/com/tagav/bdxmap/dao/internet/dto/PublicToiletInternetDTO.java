package com.tagav.bdxmap.dao.internet.dto;


public class PublicToiletInternetDTO extends CommonInternetDTO{
	private String adresse;
	private String nom;
	private String quartier;
	private String typologie;
	private String option;
	
	public String getAdresse() {
		return adresse;
	}

	public void setAdresse(String adresse) {
		this.adresse = adresse;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getQuartier() {
		return quartier;
	}

	public void setQuartier(String quartier) {
		this.quartier = quartier;
	}

	public String getTypologie() {
		return typologie;
	}

	public void setTypologie(String typologie) {
		this.typologie = typologie;
	}
	
	public String getOption() {
		return option;
	}

	public void setOption(String option) {
		this.option = option;
	}

}
