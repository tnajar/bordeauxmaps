package com.tagav.bdxmap.dao.internet.dto;


public class InternetAccessPointInternetDTO extends CommonInternetDTO{
	private String nom;
	private String situation;
	private String type_public;
	private String nb_postes;
	private String payant;
	private String type_acces;

	public String getNom() {
		return nom;
	}
	public void setNom(String nom) {
		this.nom = nom;
	}
	public String getSituation() {
		return situation;
	}
	public void setSituation(String situation) {
		this.situation = situation;
	}
	public String getType_public() {
		return type_public;
	}
	public void setType_public(String type_public) {
		this.type_public = type_public;
	}
	public String getNb_postes() {
		return nb_postes;
	}
	public void setNb_postes(String nb_postes) {
		this.nb_postes = nb_postes;
	}
	public String getPayant() {
		return payant;
	}
	public void setPayant(String payant) {
		this.payant = payant;
	}
	public String getType_acces() {
		return type_acces;
	}
	public void setType_acces(String type_acces) {
		this.type_acces = type_acces;
	}
}
