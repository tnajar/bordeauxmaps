package com.tagav.bdxmap.dao.internet.dto;


public class PublicParkingLotInternetDTO extends CommonInternetDTO{
	private String domanialite;
	private String nature;
	private String nom;
	private String nombre_de_places;

	public String getDomanialite() {
		return domanialite;
	}

	public void setDomanialite(String domanialite) {
		this.domanialite = domanialite;
	}

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getNombre_de_places() {
		return nombre_de_places;
	}

	public void setNombre_de_places(String nombre_de_places) {
		this.nombre_de_places = nombre_de_places;
	}

}
