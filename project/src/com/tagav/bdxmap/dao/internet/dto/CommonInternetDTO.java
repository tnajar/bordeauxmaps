package com.tagav.bdxmap.dao.internet.dto;

public abstract class CommonInternetDTO {
	private String partitionKey;
	private String rowKey;
	private String Timestamp;
	private String entityid;
	private String cle;
	private String x_long;
	private String y_lat;
	private String geometrie;
	private String num_quartier;

	public String getPartitionKey() {
		return partitionKey;
	}

	public void setPartitionKey(String partitionKey) {
		this.partitionKey = partitionKey;
	}

	public String getRowKey() {
		return rowKey;
	}

	public void setRowKey(String rowKey) {
		this.rowKey = rowKey;
	}

	public String getTimestamp() {
		return Timestamp;
	}

	public void setTimestamp(String timestamp) {
		Timestamp = timestamp;
	}

	public String getEntityid() {
		return entityid;
	}

	public void setEntityid(String entityid) {
		this.entityid = entityid;
	}

	public String getCle() {
		return cle;
	}

	public void setCle(String cle) {
		this.cle = cle;
	}

	public String getX_long() {
		return x_long;
	}

	public void setX_long(String x_long) {
		this.x_long = x_long;
	}

	public String getY_lat() {
		return y_lat;
	}

	public void setY_lat(String y_lat) {
		this.y_lat = y_lat;
	}

	public String getGeometrie() {
		return geometrie;
	}

	public void setGeometrie(String geometrie) {
		this.geometrie = geometrie;
	}

	public String getNum_quartier() {
		return num_quartier;
	}

	public void setNum_quartier(String num_quartier) {
		this.num_quartier = num_quartier;
	}

}
