package com.tagav.bdxmap.dao.internet.dto;


public class PlaygroundInternetDTO extends CommonInternetDTO{
	private String nature;
	private String nom;
	private String age_min;
	private String age_max;
	private String nombre_jeux;

	public String getNature() {
		return nature;
	}

	public void setNature(String nature) {
		this.nature = nature;
	}

	public String getNom() {
		return nom;
	}

	public void setNom(String nom) {
		this.nom = nom;
	}

	public String getAge_min() {
		return age_min;
	}

	public void setAge_min(String age_min) {
		this.age_min = age_min;
	}

	public String getAge_max() {
		return age_max;
	}

	public void setAge_max(String age_max) {
		this.age_max = age_max;
	}

	public String getNombre_jeux() {
		return nombre_jeux;
	}

	public void setNombre_jeux(String nombre_jeux) {
		this.nombre_jeux = nombre_jeux;
	}

}
