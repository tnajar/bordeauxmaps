package com.tagav.bdxmap.converter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;

import com.tagav.bdxmap.InternetAccessType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.InternetAccessPoint;
import com.tagav.bdxmap.dao.internet.dao.DAOException;
import com.tagav.bdxmap.dao.internet.dao.InternetAccessPointsInternetDAO;
import com.tagav.bdxmap.dao.internet.dto.InternetAccessPointInternetDTO;

public class InternetAccessPointsConverter extends CommonDataConverter {

	private static final String STRING_WIFI = "Wifi";
	private static final String STRING_PHYSICAL = "Poste libre service";

	private List<InternetAccessPointInternetDTO> internetAccessPointDataToConvert;

	@SuppressLint("DefaultLocale")
	private InternetAccessType typeAccessFormat(String type_acces) {
		InternetAccessType type = null;
		if (type_acces == null) {
			return null;
		}
		if (type_acces.toLowerCase().contains(STRING_WIFI)
				&& type_acces.toLowerCase().contains(STRING_PHYSICAL))
			type = InternetAccessType.WIFI_AND_PHYSICAL;
		else if (type_acces.toLowerCase().contains(STRING_WIFI)
				&& !type_acces.toLowerCase().contains(STRING_PHYSICAL))
			type = InternetAccessType.WIFI_ONLY;
		else if (type_acces.toLowerCase().contains(STRING_PHYSICAL)
				&& !type_acces.toLowerCase().contains(STRING_WIFI))
			type = InternetAccessType.PHYSICAL_ONLY;
		return type;
	}

	@SuppressLint("DefaultLocale")
	private boolean isPayant(String payant) {
		boolean free;
		if (payant == null) {
			return true;
		} else if (payant.toLowerCase().equals("oui"))
			free = false;
		else
			free = true;
		return free;
	}

	public List<CommonMapElement> businessFactory() throws DAOException {
		internetAccessPointDataToConvert = (new InternetAccessPointsInternetDAO())
				.getData();
		List<CommonMapElement> convertedData = new ArrayList<CommonMapElement>();
		for (InternetAccessPointInternetDTO singleRowDataToConvert : internetAccessPointDataToConvert) {
			InternetAccessPoint businessObject = new InternetAccessPoint(
					singleRowDataToConvert.getNom(),
					isPayant(singleRowDataToConvert.getPayant()),
					typeAccessFormat(singleRowDataToConvert.getType_acces()),
					convertToDouble(singleRowDataToConvert.getX_long()),
					convertToDouble(singleRowDataToConvert.getY_lat()));
			convertedData.add(businessObject);
		}
		return convertedData;

	}

}
