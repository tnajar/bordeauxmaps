package com.tagav.bdxmap.converter;

import java.util.ArrayList;
import java.util.List;

import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.Playground;
import com.tagav.bdxmap.dao.internet.dao.DAOException;
import com.tagav.bdxmap.dao.internet.dao.PlaygroundsInternetDAO;
import com.tagav.bdxmap.dao.internet.dto.PlaygroundInternetDTO;

public class PlaygroundsConverter extends CommonDataConverter {

	private List<PlaygroundInternetDTO> playgroundsDataToConvert;

	public List<CommonMapElement> businessFactory() throws DAOException {
		playgroundsDataToConvert = (new PlaygroundsInternetDAO()).getData();
		List<CommonMapElement> convertedData = new ArrayList<CommonMapElement>();
		for (PlaygroundInternetDTO singleRowDataToConvert : playgroundsDataToConvert) {

			Playground businessObject = new Playground(
					singleRowDataToConvert.getNom(),
					convertToInteger(singleRowDataToConvert.getAge_min()),
					convertToInteger(singleRowDataToConvert.getAge_max()),
					convertToInteger(singleRowDataToConvert.getNombre_jeux()),
					convertToDouble(singleRowDataToConvert.getX_long()),
					convertToDouble(singleRowDataToConvert.getY_lat()));
			convertedData.add(businessObject);
		}
		return convertedData;
	}
}
