package com.tagav.bdxmap.converter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;

import com.tagav.bdxmap.ToiletType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.PublicToilet;
import com.tagav.bdxmap.dao.internet.dao.DAOException;
import com.tagav.bdxmap.dao.internet.dao.PublicToiletsInternetDAO;
import com.tagav.bdxmap.dao.internet.dto.PublicToiletInternetDTO;

public class PublicToiletsConverter extends CommonDataConverter {

	private static final String DISABLED = "Handicapé";
	private static final String AUTOMATIC = "automatique";
	private static final String TOILET = "sanitaire";
	private static final String URINAL = "urinoir";
	private List<PublicToiletInternetDTO> publicToiletDataToConvert;

	@SuppressLint("DefaultLocale")
	private ToiletType typeToiletFormat(String typologie) {
		ToiletType type = null;
		if (typologie == null) {
			return null;
		}
		if (typologie.toLowerCase().contains(URINAL)
				&& typologie.toLowerCase().contains(TOILET))
			type = ToiletType.URINAL_AND_TOILET;
		else if (typologie.toLowerCase().contains(URINAL)
				&& !typologie.toLowerCase().contains(TOILET))
			type = ToiletType.URINAL;
		else if (typologie.toLowerCase().contains(AUTOMATIC))
			type = ToiletType.AUTOMATIC_TOILET;
		return type;
	}

	@SuppressLint("DefaultLocale")
	private boolean disabledFormat(String handicap) {
		boolean type = false;
		if (handicap == null) {
			type = false;
		} else if (handicap.toLowerCase().contains(DISABLED)) {
			type = true;
		} else {
			type = false;
		}
		return type;
	}

	public List<CommonMapElement> businessFactory() throws DAOException {
		publicToiletDataToConvert = (new PublicToiletsInternetDAO()).getData();
		List<CommonMapElement> convertedData = new ArrayList<CommonMapElement>();
		for (PublicToiletInternetDTO singleRowDataToConvert : publicToiletDataToConvert) {
			PublicToilet businessObject = new PublicToilet(
					singleRowDataToConvert.getNom(),
					typeToiletFormat(singleRowDataToConvert.getTypologie()),
					disabledFormat(singleRowDataToConvert.getOption()),
					convertToDouble(singleRowDataToConvert.getX_long()),
					convertToDouble(singleRowDataToConvert.getY_lat()));
			convertedData.add(businessObject);
		}
		return convertedData;
	}
}
