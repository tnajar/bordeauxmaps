package com.tagav.bdxmap.converter;

import java.util.List;

import android.util.Log;

import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.dao.internet.dao.DAOException;

public abstract class CommonDataConverter {

	public static String FORMAT_ERROR = "Erreur de format lors de la conversion";

	public double convertToDouble(String stringToConvert) {
		double returnVal = 0;
		try {
			returnVal = Double.valueOf(stringToConvert);
		} catch (NumberFormatException e) {
			Log.w(FORMAT_ERROR, " :: double");
		}
		return returnVal;
	}

	public int convertToInteger(String stringToConvert) {
		int returnVal = 0;
		try {
			returnVal = Integer.parseInt(stringToConvert);
		} catch (NumberFormatException e) {
			Log.w(FORMAT_ERROR, " :: integer");
		}
		return returnVal;
	}

	public abstract List<CommonMapElement> businessFactory()
			throws DAOException;

}
