package com.tagav.bdxmap.converter;

import java.util.ArrayList;
import java.util.List;

import android.annotation.SuppressLint;

import com.tagav.bdxmap.ParkingType;
import com.tagav.bdxmap.business.element.CommonMapElement;
import com.tagav.bdxmap.business.element.PublicParkingLot;
import com.tagav.bdxmap.dao.internet.dao.DAOException;
import com.tagav.bdxmap.dao.internet.dao.PublicParkingLotsInternetDAO;
import com.tagav.bdxmap.dao.internet.dto.PublicParkingLotInternetDTO;

public class PublicParkingLotsConverter extends CommonDataConverter {

	private static final String OUTDOOR = "surface";
	private static final String PAYING = "payant";
	private static final String FREE = "gratuit";
	private static final String INDOOR = "ouvrage";
	private List<PublicParkingLotInternetDTO> parkingLotsDataToConvert;

	@SuppressLint("DefaultLocale")
	private ParkingType typeParkingFormat(String type_parking) {
		ParkingType type = null;
		if (type_parking == null) {
			return null;
		}
		if (type_parking.toLowerCase().contains(INDOOR)
				&& type_parking.toLowerCase().contains(FREE))
			type = ParkingType.FREE_INDOOR;
		else if (type_parking.toLowerCase().contains(INDOOR)
				&& !type_parking.toLowerCase().contains(FREE))
			type = ParkingType.PAYING_INDOOR;
		else if (type_parking.toLowerCase().contains(OUTDOOR)
				&& type_parking.toLowerCase().contains(PAYING))
			type = ParkingType.PAYING_OUTDOOR;
		else if (type_parking.toLowerCase().contains(OUTDOOR)
				&& type_parking.toLowerCase().contains(FREE))
			type = ParkingType.FREE_OUTDOOR;
		return type;
	}

	public List<CommonMapElement> businessFactory() throws DAOException {
		parkingLotsDataToConvert = (new PublicParkingLotsInternetDAO())
				.getData();
		List<CommonMapElement> convertedData = new ArrayList<CommonMapElement>();
		for (PublicParkingLotInternetDTO singleRowDataToConvert : parkingLotsDataToConvert) {
			PublicParkingLot businessObject = new PublicParkingLot(
					singleRowDataToConvert.getNom(),
					convertToInteger(singleRowDataToConvert
							.getNombre_de_places()),
					typeParkingFormat(singleRowDataToConvert.getNature()),
					convertToDouble(singleRowDataToConvert.getX_long()),
					convertToDouble(singleRowDataToConvert.getY_lat()));
			convertedData.add(businessObject);
		}
		return convertedData;
	}
}
